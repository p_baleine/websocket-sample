var io = require('socket.io').listen(3001);

io.sockets.on("connection", function(socket) {
  socket.on("message", function(msg) {
    socket.broadcast.emit("message", msg);
  });
});
